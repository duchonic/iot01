# build

The buld should get a output like this

```
 *  Executing task in folder L475: C:\Users\SWEngineer\.platformio\penv\Scripts\platformio.exe run --environment disco_l475vg_iot01a 

Processing disco_l475vg_iot01a (platform: ststm32; board: disco_l475vg_iot01a; framework: arduino)
Verbose mode can be enabled via `-v, --verbose` option
CONFIGURATION: https://docs.platformio.org/page/boards/ststm32/disco_l475vg_iot01a.html
PLATFORM: ST STM32 (17.3.0) > ST B-L475E-IOT01A Discovery kit
HARDWARE: STM32L475VGT6 80MHz, 96KB RAM, 1MB Flash
DEBUG: Current (stlink) On-board (stlink) External (blackmagic, cmsis-dap, jlink)
PACKAGES: 
 - framework-arduinoststm32 @ 4.20701.0 (2.7.1) 
 - framework-cmsis @ 2.50900.0 (5.9.0) 
 - toolchain-gccarmnoneeabi @ 1.120301.0 (12.3.1)
LDF: Library Dependency Finder -> https://bit.ly/configure-pio-ldf
LDF Modes: Finder ~ chain, Compatibility ~ soft
Found 12 compatible libraries
Scanning dependencies...
No dependencies
Building in release mode
Checking size .pio\build\disco_l475vg_iot01a\firmware.elf
Advanced Memory Usage is available via "PlatformIO Home > Project Inspect"
RAM:   [          ]   1.3% (used 1288 bytes from 98304 bytes)
Flash: [          ]   1.4% (used 14900 bytes from 1048576 bytes)
Building .pio\build\disco_l475vg_iot01a\firmware.bin
======== [SUCCESS] Took 2.81 seconds ===
 *  Terminal will be reused by tasks, press any key to close it. 
```